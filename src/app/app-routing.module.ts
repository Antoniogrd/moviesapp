import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "",
    loadChildren: () => import('./pages/home-page/home-page.module').then(m => m.HomePageModule) 
  },
  {
    path: "favoritos",
    loadChildren: () => import('./pages/favorites-movies-page/favorites-movies-page.module').then(m => m.FavoritesMoviesPageModule)
  },
  {
    path: "pendientes",
    loadChildren: () => import('./pages/pending-movies-page/pending-movies-page.module').then(m => m.PendingMoviesPageModule)
  },
  {
    path: "vistas",
    loadChildren: () => import('./pages/watched-movies-page/watched-movies-page.module').then(m => m.ViewesMoviesPageModule)
  },
  {
    path: "pelicula/:id",
    loadChildren: () => import('./pages/detail-movie-page/detail-movie-page.module').then(m => m.DetailMoviePageModule)
  },
  {
    path: "**",
    redirectTo: ""
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
