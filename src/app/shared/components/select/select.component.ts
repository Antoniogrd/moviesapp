import { Component, OnInit, SimpleChanges } from '@angular/core';
import { MovieListService } from '../../services/select.service';


@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
    
  
  select: any[] = []; 

  constructor(private selectService: MovieListService){}
  

  ngOnInit(): void {
    this.select = [
      {text: "Favorito", image: "pi pi-star", image2: "pi pi-star-fill", actualImage:"pi pi-star", valor: false },
      {text:"Pendiente", image: "pi pi-flag", image2: "pi pi-flag-fill", actualImage:"pi pi-flag", valor: false },
      {text:"Vista", image: "pi pi-eye", image2: "pi pi-eye-slash", actualImage:"pi pi-eye", valor: false }
    ];
  }


  changeSelectValue(item: any) {
    item.valor = !item.valor;
    
    if (item.valor) {
      item.actualImage = item.image2;
    }
    else {
      item.actualImage = item.image;
    }
    
    if (
      (item.text === "Vista") &&
      item.valor
    ) {
      for (let elem of this.select) {
        if (elem.text === "Pendiente") {	// el que buscamos
          elem.valor = false;
          elem.actualImage = elem.image;
          
          break;	// Como hemos utilizado un for, salimos una vez que hemos encontrado el elemento que buscábamos
        }}};

    if (
      (item.text === "Pendiente") &&
      item.valor
    ) {
      for (let elem of this.select) {
        if (elem.text === "Vista") {	// el que buscamos
          elem.valor = false;
          console.log("hola")
          elem.actualImage = elem.image;
          
          break;	// Como hemos utilizado un for, salimos una vez que hemos encontrado el elemento que buscábamos
        }}};
  }
  
  };



