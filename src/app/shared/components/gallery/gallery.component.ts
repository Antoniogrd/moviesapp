import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MovieDataWithRating } from '../../interfaces/movieDataWithRaing.class';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  @Input() movie: MovieDataWithRating;
  @Output() clickRating: EventEmitter<void>;
  baseUrlImg: string = "https://image.tmdb.org/t/p/w185_and_h278_bestv2";
  selectValue: boolean;
  showSelect: boolean = false;


  constructor() {
    this.selectValue = false;
    this.clickRating = new EventEmitter<void>();
   }

  ngOnInit(): void {
    if(window.location.href==="http://localhost:4200/"){
    this.showSelect = true;
    }
    
}

  selected(){
    if(this.selectValue === false){
      this.selectValue = true;    
    } else if(this.selectValue === true){
      this.selectValue = false;
    }
  }

  clickFavorite(movie: MovieDataWithRating){
    movie.toggleFavorite();
    this.clickRating.emit();
  }

  clickPending(movie: MovieDataWithRating){
    movie.togglePending();
  this.clickRating.emit();
  }

  clickWatched(movie: MovieDataWithRating){
    movie.toggleWatched();
    this.clickRating.emit();
  }

}



