import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GalleryComponent } from './components/gallery/gallery.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SelectComponent } from './components/select/select.component';
//import { PrimeIcons } from 'primeng/api';




@NgModule({
  declarations: [
    GalleryComponent,
    SelectComponent
  ],
  exports: [GalleryComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    
  ]
})
export class SharedModule { }
