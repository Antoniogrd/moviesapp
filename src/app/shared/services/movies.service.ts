import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private apiKey: string = environment.API_KEY
  baseUrl: string = "https://api.themoviedb.org/3/"
  movieListUrl: string = "discover/movie?sort_by=popularity.desc" 
  

  constructor(private http: HttpClient ) { }

  getMovies(){
    return this.http.get(`${this.baseUrl}${this.movieListUrl}&api_key=${this.apiKey}&language=es`)
  }

  getMovie(id: string){
    return this.http.get(`${this.baseUrl}movie/${id}?api_key=${this.apiKey}&language=es-ES`)
  }


}
