import { Injectable } from '@angular/core';
import { MovieDataWithRating } from '../interfaces/movieDataWithRaing.class';

@Injectable({
  providedIn: 'root'
})
export class MovieListService {
  public movieList: Array<MovieDataWithRating>;

  constructor(){}

}


