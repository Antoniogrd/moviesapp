export interface Movie {
    page:          number;
    results:       MovieData[];
    total_pages:   number;
    total_results: number;
}

export class MovieData {
    constructor(
    public    adult:             boolean,
    public    backdrop_path:     string,
    public    genre_ids:         number[],
    public    id:                number,
    public    original_language: string,
    public    original_title:    string,
    public    overview:          string,
    public    popularity:        number,
    public    poster_path:       string,
    public    release_date:      Date,
    public    title:             string,
    public    video:             boolean,
    public    vote_average:      number,
    public    vote_count:        number,
    ){}
}




