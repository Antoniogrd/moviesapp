
export interface MovieState {
    movieId: number;
    movieTitle: string;
    favorite: boolean;
    pending: boolean;
    watched: boolean;
}