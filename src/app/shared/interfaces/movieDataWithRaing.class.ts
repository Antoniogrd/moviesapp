import { MovieData } from "./movies.interface";

export class MovieDataWithRating extends MovieData {
    constructor(       
        public    adult:             boolean,
        public    backdrop_path:     string,
        public    genre_ids:         number[],
        public    id:                number,
        public    original_language: string,
        public    original_title:    string,
        public    overview:          string,
        public    popularity:        number,
        public    poster_path:       string,
        public    release_date:      Date,
        public    title:             string,
        public    video:             boolean,
        public    vote_average:      number,
        public    vote_count:        number,
        public    favorite:          boolean,
        public    pending:           boolean,
        public    watched:           boolean,
        ){
            super(adult, backdrop_path, genre_ids, id, original_language, original_title, overview, popularity, poster_path, release_date, title, video, vote_average, vote_count)
        }

        setFavorite(state:boolean){
            this.favorite = state;
        }

        setPending(state:boolean){
            this.pending = state;
            
            if(this.pending){
                this.watched = false;
            }
        }

        setWatched(state:boolean){
            this.watched = state;

            if(this.watched){
                this.pending = false;
            }
        }

        setState(favorite:boolean, pending:boolean, watched:boolean){
            this.setFavorite(favorite);
            this.setPending(pending);
            this.setWatched(watched);
        }

        toggleFavorite(){
            this.setFavorite(!this.favorite);
        }

        togglePending(){
            this.setPending(!this.pending);
        }
        
        toggleWatched(){
            this.setWatched(!this.watched);
        }
}


