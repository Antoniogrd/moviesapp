import { Component } from '@angular/core';

interface MenuItem {
  route: string;
  name: string;
}

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})

export class MenuComponent {

  MenuItems: MenuItem[] = [
    { route: "/", name: "Home" },
    { route: "/favoritos", name: "Favoritos" },
    { route: "/pendientes", name: "Pendientes" },
    { route: "/vistas", name: "Vistas" }
  ]

}
