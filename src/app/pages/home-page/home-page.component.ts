import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../shared/services/movies.service';
import { take } from 'rxjs';
import { MovieDataWithRating } from '../../shared/interfaces/movieDataWithRaing.class';
import { MovieState } from 'src/app/shared/interfaces/movieState.interface';
import { MovieData } from 'src/app/shared/interfaces/movies.interface';
import { MovieListService } from '../../shared/services/select.service';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {  

  constructor(
    private moviesService: MoviesService,
    public _movieListService: MovieListService) {}

  ngOnInit(): void {
    
    this.moviesService.getMovies().pipe(take(1)).subscribe((res:any) => {
      const movieArray: MovieData[] = res.results;
      const movieArrayWithRating: MovieDataWithRating[] = [];
      movieArray.forEach((m) => {
        movieArrayWithRating.push(new MovieDataWithRating(
          m.adult,
          m.backdrop_path,
          m.genre_ids,
          m.id,
          m.original_language,
          m.original_title,
          m.overview,
          m.popularity,
          m.poster_path,
          m.release_date,
          m.title,
          m.video,
          m.vote_average,
          m.vote_count,
          false,
          false,
          false
        ));
      });
      this._movieListService.movieList = movieArrayWithRating;
      this.loadState();
    });
    
  }

saveState(){
  const moviesState: Array<MovieState> = [];

  this._movieListService.movieList.forEach((movie: MovieDataWithRating) => {
    if(movie.favorite || movie.pending || movie.watched){
      moviesState.push({
        movieId: movie.id, 
        movieTitle: movie.title, 
        favorite: movie.favorite, 
        pending: movie.pending, 
        watched: movie.watched
      })
    }
  })
  const movieStateJson = JSON.stringify(moviesState);
  localStorage.setItem("movieState", movieStateJson);
}

loadState(){
    let movieStateJson: string = localStorage.getItem("movieState") || "[]"
    if(!movieStateJson || movieStateJson === ""){
      movieStateJson = "[]";
    }
    const moviesState: Array<MovieState> = JSON.parse(movieStateJson);

    moviesState.forEach((movieState: MovieState) => {
      for(let movie of this._movieListService.movieList){
        if(movieState.movieId === movie.id){
          movie.favorite = movieState.favorite;
          movie.pending = movieState.pending;
          movie.watched = movieState.watched;
          break
        } 
      }
    })
    
}
}

