import { Component, OnInit } from '@angular/core';
import { MovieListService } from '../../shared/services/select.service';

@Component({
  selector: 'app-pending-movies-page',
  templateUrl: './pending-movies-page.component.html',
  styleUrls: ['./pending-movies-page.component.scss']
})
export class PendingMoviesPageComponent implements OnInit {

  constructor(public _movieListService: MovieListService) { }

  ngOnInit(): void {}

}
