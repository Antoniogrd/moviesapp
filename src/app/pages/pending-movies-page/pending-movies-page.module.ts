import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PendingMoviesPageRoutingModule } from './pending-movies-page-routing.module';
import { PendingMoviesPageComponent } from './pending-movies-page.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    PendingMoviesPageComponent    
  ],
  imports: [
    CommonModule,
    PendingMoviesPageRoutingModule,
    SharedModule
  ]
})
export class PendingMoviesPageModule { }
