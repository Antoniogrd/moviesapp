import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailMoviePageRoutingModule } from './detail-movie-page-routing.module';
import { DetailMoviePageComponent } from './detail-movie-page.component';


@NgModule({
  declarations: [
    DetailMoviePageComponent
  ],
  imports: [
    CommonModule,
    DetailMoviePageRoutingModule
  ]
})
export class DetailMoviePageModule { }
