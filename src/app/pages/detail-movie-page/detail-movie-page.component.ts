import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from '../../shared/services/movies.service';

@Component({
  selector: 'app-detail-movie-page',
  templateUrl: './detail-movie-page.component.html',
  styleUrls: ['./detail-movie-page.component.scss']
})
export class DetailMoviePageComponent implements OnInit {
  
  movieData: any = {};
  poster_url: string = "https://image.tmdb.org/t/p/w185_and_h278_bestv2/";
  poster_path: string = "";
  
  constructor(private route: ActivatedRoute, private moviesService: MoviesService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const id = params.get("id");

      this.moviesService.getMovie(id!).subscribe(res => {
        this.movieData = res
        this.poster_path = this.poster_url + this.movieData.poster_path;
        
      })
      
    })
  }

  
  

}
