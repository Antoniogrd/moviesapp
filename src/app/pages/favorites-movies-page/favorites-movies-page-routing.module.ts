import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FavoritesMoviesPageComponent } from './favorites-movies-page.component';

const routes: Routes = [
  {
    path:"",
    component: FavoritesMoviesPageComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FavoritesMoviesPageRoutingModule { }
