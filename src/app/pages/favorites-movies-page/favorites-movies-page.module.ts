import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FavoritesMoviesPageRoutingModule } from './favorites-movies-page-routing.module';
import { FavoritesMoviesPageComponent } from './favorites-movies-page.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    FavoritesMoviesPageComponent
  ],
  imports: [
    CommonModule,
    FavoritesMoviesPageRoutingModule,
    SharedModule
  ]
})
export class FavoritesMoviesPageModule { }
