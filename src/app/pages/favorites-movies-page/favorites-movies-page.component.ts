import { Component, OnInit } from '@angular/core';
import { MovieListService } from '../../shared/services/select.service';
import { MovieDataWithRating } from '../../shared/interfaces/movieDataWithRaing.class';

@Component({
  selector: 'app-favorites-movies-page',
  templateUrl: './favorites-movies-page.component.html',
  styleUrls: ['./favorites-movies-page.component.scss']
})
export class FavoritesMoviesPageComponent implements OnInit {

  constructor(public _movieListService: MovieListService) { }


  ngOnInit(): void {}

}
