import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WatchedMoviesPageComponent } from './watched-movies-page.component';

const routes: Routes = [
  {
    path:'',
    component: WatchedMoviesPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WatchedMoviesPageRoutingModule { }
