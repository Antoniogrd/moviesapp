import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WatchedMoviesPageRoutingModule } from './watched-movies-page-routing.module';
import { WatchedMoviesPageComponent } from './watched-movies-page.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    WatchedMoviesPageComponent
  ],
  imports: [
    CommonModule,
    WatchedMoviesPageRoutingModule,
    SharedModule
  ]
})
export class ViewesMoviesPageModule { }
