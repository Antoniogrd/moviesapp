import { Component, OnInit } from '@angular/core';
import { MovieListService } from '../../shared/services/select.service';

@Component({
  selector: 'app-watched-movies-page',
  templateUrl: './watched-movies-page.component.html',
  styleUrls: ['./watched-movies-page.component.scss']
})
export class WatchedMoviesPageComponent implements OnInit {

  constructor(public _movieListService: MovieListService) { }

  ngOnInit(): void {}

}
